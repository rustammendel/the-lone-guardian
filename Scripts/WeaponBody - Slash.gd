extends KinematicBody2D
#SLASHER
var sa_damage = 5
onready var damage = sa_damage * get_parent().sharpness


func _ready():
	pass  # Replace with function body.


func give_damage():
	return damage * get_parent().get_momentum() / 5000.0
