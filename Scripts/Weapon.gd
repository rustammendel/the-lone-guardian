extends Sprite


export var speed = 5
export var rot_speed = 2
export var weight = 1
export var player_control = false
var size = Vector2(1496, 304) * scale
var control_point
var velocity = Vector2(0, 0)
var momentum = 0
var prev_momentum  # used for logging only
var rear_pos
var sharpness = 1

# variables for body interaction
var shoulder_path = "Body/Shoulder"
var grav_point
var _range = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	size = texture.get_size() * scale
	control_point = global_position
	momentum = 0
	rear_pos = get_parent().position
	prev_momentum = momentum


func align_handle(change = Vector2(0, 0)):
	velocity = change
	rear_pos = get_parent().global_position

	if player_control:
		if Input.is_action_pressed('lower'):
			velocity.y += 5

		if Input.is_action_pressed('raise'):
			velocity.y -= 5

	rear_pos += velocity * speed * 2
	var dist = (rear_pos - get_parent().global_position) / 10 * speed
	if is_in_range(dist):
		get_parent().translate(dist)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	gravitate()
	align_handle()

	# this part should be removed when proper stance triggering is implemented
	if Input.is_action_pressed('ui_up'):
		up_stance()
	if Input.is_action_pressed('ui_right'):
		right_stance()
	if Input.is_action_pressed('ui_down'):
		down_stance()
	if Input.is_action_pressed('ui_left'):
		left_stance()
	if Input.is_action_pressed('lower') and ! player_control:
		low_guard()
	if Input.is_action_pressed("raise") and ! player_control:
		high_guard()
	# this part should be removed when proper stance triggering is implemented

	align_weapon(delta)


#	if momentum > prev_momentum:
#		print(momentum)
#		prev_momentum = momentum


func align_weapon(delta):
	var control_pos = get_control_point()
	var distance_from_mouse = control_pos - global_position

	var weapon_direction = get_parent().rotation

	var weapon_direction_vector = Vector2(
		1 * cos(get_parent().rotation), 1 * sin(get_parent().rotation)
	)
	var direction_difference = weapon_direction_vector.angle_to(control_pos - global_position)

#	get_parent().get_node("Log").text = str(weapon_direction)
#	get_parent().get_node("Log").rect_rotation = -get_parent().rotation_degrees

	var rot_distance = direction_difference / 10 * rot_speed
	get_parent().rotate(rot_distance)
	if ! is_in_range(Vector2(0, 0)):
		get_parent().rotate(-rot_distance)

	var prev_position = get_parent().global_position
	var tra_distance = distance_from_mouse / 50 * speed
	if distance_from_mouse.length() > size.x / 2:
		if is_in_range(tra_distance):
			get_parent().translate(tra_distance)
			rear_pos += tra_distance
	if distance_from_mouse.length() < size.x / 2.2:
		if is_in_range(-tra_distance):
			get_parent().translate(-tra_distance)
			rear_pos -= tra_distance

	momentum = calc_momentum(rot_distance, delta, get_parent().position, prev_position, weight)


func get_control_point():
	if player_control:
		return get_viewport().get_mouse_position()
	else:
		return control_point


func realign(target):
	control_point = target


func up_stance():
	control_point = global_position - Vector2(0, size.x / 2)


func right_stance():
	control_point = global_position + Vector2(size.x / 2, 0)


func down_stance():
	control_point = global_position + Vector2(0, size.x / 2)


func left_stance():
	control_point = global_position - Vector2(size.x / 2, 0)


func low_guard():
	if (rear_pos - get_parent().global_position).length() < 1:
		align_handle(Vector2(0, 10))


func high_guard():
	if (rear_pos - get_parent().global_position).length() < 1:
		align_handle(Vector2(0, -10))


func gravitate():
	var difference = grav_point - get_parent().global_position
	var diff_len = difference.length()

	if is_in_range(difference):
		get_parent().translate(difference / 10)


func is_in_range(move_vector):
	var difference_1 = (
		(get_node("Hand1").global_position + move_vector)
		- get_parent().get_parent().get_node(shoulder_path + "1").global_position
	)
	var diff_len_1 = difference_1.length()
	var difference_2 = (
		(get_node("Hand2").global_position + move_vector)
		- get_parent().get_parent().get_node(shoulder_path + "2").global_position
	)
	var diff_len_2 = difference_2.length()
	if diff_len_1 > _range or diff_len_2 > _range:
		return false
	else:
		return true


func calc_momentum(rot_distance, delta, par_position, prev_position, weight):
	return (
		(abs(rot_distance / delta) + abs((par_position - prev_position).length() / delta))
		* weight
	)


func get_momentum():
	return momentum
