extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass  # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if get_node("Enemy").HP <= 0:
		get_node("Enemy").translate(Vector2(1500, 0))
		get_node("Enemy").HP = 100
