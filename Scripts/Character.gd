extends Node2D

# Declare member variables here. Examples:
# var a = 2
export var HP = 100


func damage(hp):
	HP -= hp


# Called when the node enters the scene tree for the first time.
func _ready():
	pass  # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	if body.get_parent().get_parent() != get_node("Body").get_parent().get_node("WeaponControl"):
		if body.has_method("give_damage"):
			damage(body.give_damage())
			print(HP)
