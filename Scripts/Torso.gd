extends MeshInstance2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var _range = 200
export var player = false
var time_start = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	time_start = OS.get_unix_time()
	get_parent().get_node("WeaponControl/Weapon")._range = _range
	if player:
		get_parent().get_node("WeaponControl/Weapon").grav_point = get_node(
			"RestPos"
		).global_position
	else:
		get_parent().get_node("WeaponControl/Weapon").grav_point = get_node(
			"RestPos2"
		).global_position
	get_parent().get_node("WeaponControl/Weapon").player_control = player


func aproach(target):
	var difference = (get_parent().global_position - target).x
	var norm_dif = 1
	if difference > 0:
		norm_dif = -1
	if difference > _range * 1.5:
		get_parent().translate(Vector2(norm_dif, 0))


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	var time_now = OS.get_unix_time()
	var elapsed = time_now - time_start

	if ! player:
		var weapon_node = get_parent().get_node("WeaponControl/Weapon")
		var player_node = get_parent().get_parent().get_node("Player")
		var target = player_node.global_position

		if elapsed % 2 == 0:
			weapon_node.realign(
				get_parent().get_parent().get_node("Player").get_node(
					"WeaponControl/Weapon/Hand1"
				).global_position
			)
		elif elapsed % 7 == 0:
			weapon_node.realign(player_node.global_position)
		elif elapsed % 8 == 0:
			weapon_node.realign((player_node.global_position + global_position) / 2)
		aproach(target)
