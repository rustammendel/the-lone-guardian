# The Lone Guardian
The Lone Guardian is a game project for a university subject.
The game is a two-dimensional fighting game, where the player's mouse controls the weapon of their character and the goal is to survive as long as you can against waves of attackers.

## Commit criteria
 - Make sure that every file you wish to commit is inside a directory that matches the category of the file (create a new directory if none match the file)
 - Be sure to mention what changed when commiting (at least write Bugfixes for XY)
 - Don't commit directly into the master branch (except in rare cases like modifying this README or deleting files that aren't needed)
 
## CI information
 - When creating a new branch, make sure to edit the .gitlab-ci.yml file: the BRANCH_NAME should be the name of your newly created branch
 - If your branch was created before CI implementation, add the following files (from the master branch) to your root directory : .gitlab-ci.yml; Dockerfile; export_presets.cfg


## Tests
 - All test scripts should be placed in the Tests folder with a name starting with "test_"
 - An example test file is inside the Tests folder for basic help
 - To use the testing feature, you'll need to add the "addons" folder from the master branch to your branch (If you created the branch before the implementation of the testing)
 - So far, all test functions should be 100% correct (no typos or wrong calls), or the tester will start an endless cicle (so wait for the tests to finish to make sure it doesn't run forever)


## Joining the project
The project (as of yet) uses the Godot engine: https://godotengine.org/

### Step 1
After downloading and installing the engine, start it up and you'll see a window with the following options on the right side:

Edit, Run, Scan, New Project, Import, Rename, Remove

### Step 2
Select Scan and locate the directory where the project files are.

Alternatively, select import and locate the project.godot file in the project directory.

### Step 3
After both of these methods, the project, named 'Project 20' should appear in the launcher window.

Select Edit, which will open up the Godot Engine.